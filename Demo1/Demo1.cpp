// Demo1.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "dllHelper.h"
#include "common.h"

/*
	行情抓取
*/

#ifdef _DEBUG
CDllHelper _dll("GSQuoteD.dll");
#else
CDllHelper _dll("GSQuote.dll");
#endif

typedef IGSQuote*(*CreateGSQuuote)();
typedef void(*ReleaseGSQuuote)(IGSQuote *pQuote);

CreateGSQuuote _create;
ReleaseGSQuuote _release;

int _tmain(int argc, _TCHAR* argv[])
{
	_create = _dll.GetProcedure<CreateGSQuuote>("CreateObject");
	_release = _dll.GetProcedure<ReleaseGSQuuote>("ReleaseObject");

	IGSQuote * pQuote = _create();

	pQuote->LoadStockPool("Table.txt");
	pQuote->Run();
	getchar();
	pQuote->Stop();
	_release(pQuote);

	return 0;
}


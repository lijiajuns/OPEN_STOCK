/*
Navicat SQLite Data Transfer

Source Server         : gsquote
Source Server Version : 30706
Source Host           : :0

Target Server Type    : SQLite
Target Server Version : 30706
File Encoding         : 65001

Date: 2016-09-26 15:41:58
*/

PRAGMA foreign_keys = OFF;

-- ----------------------------
-- Table structure for "main"."quote_0"
-- ----------------------------
DROP TABLE "main"."quote_0";
CREATE TABLE "quote_0" (
"id"  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL DEFAULT 0,
"stid"  TEXT(11) NOT NULL,
"stname"  TEXT(11) NOT NULL,
"volume"  INTEGER,
"new_price"  TEXT(11),
"time"  TEXT(33),
"timestamp"  INTEGER
);

-- ----------------------------
-- Records of quote_0
-- ----------------------------

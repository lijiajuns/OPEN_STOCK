// 下列 ifdef 块是创建使从 DLL 导出更简单的
// 宏的标准方法。此 DLL 中的所有文件都是用命令行上定义的 ZQUNTSTRATEGY_EXPORTS
// 符号编译的。在使用此 DLL 的
// 任何其他项目上不应定义此符号。这样，源文件中包含此文件的任何其他项目都会将
// ZQUNTSTRATEGY_API 函数视为是从 DLL 导入的，而此 DLL 则将用此宏定义的
// 符号视为是被导出的。
#ifdef ZQUNTSTRATEGY_EXPORTS
#define ZQUNTSTRATEGY_API __declspec(dllexport)
#else
#define ZQUNTSTRATEGY_API __declspec(dllimport)
#endif

/************************************************************************/
/* 
	关于ZQuant策略：
	1.量价齐升“突”，日内短线交易
	2.联系作者zyyoung@sohu.com
*/
/************************************************************************/

// 此类是从 ZQuntStrategy.dll 导出的
class CZQuntStrategy {
public:
	CZQuntStrategy(void);
	// TODO:  在此添加您的方法。
};

ZQUNTSTRATEGY_API int CreateObject(void);
ZQUNTSTRATEGY_API int ReleaseObject(void);

// ZQuntStrategy.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "ZQuntStrategy.h"


// 这是已导出类的构造函数。
// 有关类定义的信息，请参阅 ZQuntStrategy.h
CZQuntStrategy::CZQuntStrategy()
{
	return;
}


// 这是导出函数的一个示例。
ZQUNTSTRATEGY_API int CreateObject(void)
{
	return 42;
}

ZQUNTSTRATEGY_API int ReleaseObject(void)
{
	return 42;
}
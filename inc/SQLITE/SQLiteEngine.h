#pragma once
#include "common.h"

class sqlite3;

class CSQLiteEngine
{
public:
	CSQLiteEngine(void);
	~CSQLiteEngine(void);

	BOOL   OpenDatabase(LPCSTR lpszFileName,int nFlag,LPCSTR pwd="");
	BOOL   IsOpen();
	BOOL   Close();
	BOOL   ExecuteSQL(LPCSTR lpszSQL,SQLITE_CALLBACK pCallback,void *pUnuse);
	LPCSTR GetLastError();
	int    GetLastErrorCode(){return m_nLastErrorCode;}

protected:
	sqlite3  *m_pDatabase;
	char     *m_pLastError;
	int       m_nLastErrorCode;

};

#pragma once

#include <Windows.h>

typedef int (*SQLITE_CALLBACK)(void*,int,char**, char**);

enum DBOpenFlag
{
	DBOpenFlag_ReadOnly=1,
	DBOpenFlag_ReadWrite=2,
	DBOpenFlag_Create=4,
};

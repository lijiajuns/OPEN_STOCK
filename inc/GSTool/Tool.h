#pragma once
#include "common.h"
#include "SQLITE/SQLiteEngine.h"


typedef size_t(*SinaCallBack)(char *ptr, size_t size, size_t nmemb, string *stream);
typedef void (*ThreadPoolCallBack)(const int index);
typedef int(*sqliteCall)(void *pvoid, int argc, char **argv, char **azColName);

class CTool
{
private:
	CTool(const CTool&);
	CTool& operator=(const CTool&);
public:
	CTool();
	~CTool();
public:

	void GetCodeWithPrefix(char *stockID, char *ret);
	void GB2312ToUtf8(const char *file, char *szBufOut, int nOutSize);
	//时间戳互换:"%d-%d-%d %d:%d:%d"
	int get_timestamp(char *strIn);
public:
	//sina数据解析
	int SinaDataParser(char *sz, int len, tagSinaTick*pTick);
	//拉去行情
	int PullQuote(char *szCode, SinaCallBack pCall);
public:	
	//存储sqlie
	bool ConnectSQLite();
	void StorgeStock(tagSinaTick *pData);
	int ExecSql(char *szSQL, sqliteCall pCall, void *pCallObject);
private:
	CSQLiteEngine m_sqlite;

};

#define GSTOOL singleton_t<CTool>::instance() 

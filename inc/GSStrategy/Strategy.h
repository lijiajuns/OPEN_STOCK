#pragma once

#include <vector>
#include <map>
using namespace std;

typedef int STOCK_CYCLE;	//周期s
typedef int STOCK_BEGIN;	//采样开始日期时间戳


enum enmSQLITE
{
	SQL_CODE = 0,
	SQL_VOLUME,
	SQL_PRICE,
	SQL_TIMESTAMP,

	SQL_MAX,
};


struct tagStorgeItem
{
	float price;
	int volume;
	int timestamp;
};

//基础定义
#define SPEED_VOLUME	1.0f
#define SPEED_PRICE		1.0f

#define SPEED_VOLUME_OFFSET		0.5f
#define SPEED_PRICE_OFFSET		(SPEED_VOLUME_OFFSET / 10 + 0.001f)//0.08f


typedef vector<tagStorgeItem> vStorgeItem;
typedef map<string, vStorgeItem> mStorgeItem;
typedef mStorgeItem::iterator mStorgeItemIter;

struct tagWeight
{
	float volume_weight;	//得分
	float price_weight;		//得分
};


typedef map<string, tagWeight> mWeight;
typedef mWeight::iterator mWeightIter;
typedef pair<string, tagWeight> PAIR;
typedef vector<PAIR>vPAIR;

class IGSStrategy
{
public:
	virtual int Analyse(STOCK_BEGIN nBegin, STOCK_CYCLE cycle, char *szOutFile) = 0;
};
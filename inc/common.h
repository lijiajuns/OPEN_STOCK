#pragma once
#include <vector>
#include <string>
#include "Single.h"
using namespace std;

#define SINA_QUOTE	1
//#define EASTMONEY_QUOTE	1

#define STOCK_SINA_URL			("http://hq.sinajs.cn/list=")
#define STOCK_EASTMONEY_URL		("http://hq.sinajs.cn/list=")

#define SQLITE_DB "gsquote.db"

#define PREFIX_SZ_STOCK	"sz"	//深市代码前缀
#define PREFIX_SH_STOCK	"sh"	//沪市

#define STR_LEN     33
#define FB_NUMS     5

#define MAX_LINE	256

#define THREAD_MAX_COUNT	2
#define THREAD_SLEEP		3
#define INTERVAL_QUOTE			(1000 * 5)	//5s更新一次行情

typedef vector<string> vString;
typedef vString::iterator vStringIter;

enum
{
	SUCCESS = 0,
	FAIL,
};

//sqlite分档
enum enIndex
{
	NAME = 0,
	OPEN,
	CLOSE,
	PRICE,
	HIGH,
	LOW,
	ASK1,
	BID1,
	VOLUME,
	AMT,
	DAY = AMT + 21,
	TIME,
	OTHER,
};

//一档价量
struct tagFB
{
	int     volume;
	float   fPrice;
};

struct tagSinaTick
{
	char		szCode[STR_LEN];	  //代码
	char		name[STR_LEN];        //股票名称
	float		fOpen;                //今日开盘价
	float		fLastClose;           //昨日收盘价
	float		fNowPrice;            //当前价格
	float		fHigh;                //今日最高
	float		fLow;                 //今日最低
	float		fJHAsk1;              //集合竞价卖1价
	float		fJHBid1;              //集合竞价买1价
	int			nVolume;              //成交量（含*100）
	float		fAMT;                 //成交量
	tagFB		fb[2][FB_NUMS];       //5档行情
	char		date[STR_LEN];        //日期
	char		time[STR_LEN];        //时间
	char		offset[STR_LEN];      //保留字符串
	int			timestamp;			  //时间戳

};

//加载股票池（东财导出）
struct tagStockItem
{
	int index;
	char id[STR_LEN];
	char szName[STR_LEN];
	int  timetick;//时间戳，5分钟提交一次数据到sqlite
};
typedef vector<tagStockItem> vStockItem;

//核心库导出接口
class IGSQuote
{
public:
	virtual int LoadStockPool(char *file) = 0;
	virtual int Run() = 0;
	virtual int Stop() = 0;

};
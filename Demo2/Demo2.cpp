// Demo2.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "GSStrategy/Strategy.h"
#include "common.h"
#include "dllHelper.h"
#include "GSTool/Tool.h"
#ifdef _DEBUG
#pragma comment(lib,"../lib/GSToolD.lib")
#else
#pragma comment(lib,"../lib/GSTool.lib")
#endif

/*

	策略分析
	1.抓牛股
	2.找卖点

*/

#ifdef _DEBUG
CDllHelper _dll("GSStrategyD.dll");
#else
CDllHelper _dll("GSStrategy.dll");
#endif

typedef IGSStrategy*(*CreateGSStrategy)();
typedef void(*ReleaseGSStrategy)(IGSStrategy *pQuote);

CreateGSStrategy _create;
ReleaseGSStrategy _release;

#define OUT_FILE ("out.txt")
#define CYCLE_TIME	900				//采样周期15分钟
#define BEGIN_TIME	1474947183		//分析开始日期

int _tmain(int argc, _TCHAR* argv[])
{
	USES_CONVERSION;

	if (argc != 3)
	{
		printf("请输入正确参数,如2016-08-01 900\n");
		getchar();
		return 0;
	}

	setlocale(LC_ALL, "chs");
	int nBegin = BEGIN_TIME;
	int nCycle = BEGIN_TIME;
	TCHAR szStartTimeDate[MAX_PATH];
	swprintf_s(szStartTimeDate, MAX_PATH, L"%s 00:00:00", argv[1]);
	nBegin = GSTOOL->get_timestamp(T2A(szStartTimeDate));
	nCycle = atoi(T2A(argv[2]));

	_create = _dll.GetProcedure<CreateGSStrategy>("CreateObject");
	_release = _dll.GetProcedure<ReleaseGSStrategy>("ReleaseObject");

	IGSStrategy * pStrategy = _create();
	
	if (SUCCESS != pStrategy->Analyse(nBegin, nCycle, OUT_FILE))
	{
		printf("没有满足条件数据\n");
	}
	else
	{
		printf("分析完成,查看输出文件\n");
	}
	
	getchar();

	return 0;
}


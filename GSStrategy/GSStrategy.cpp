// GSStrategy.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "GSStrategy.h"
#include "common.h"

CGSStrategy * gGSStrategy = 0;

// 这是已导出类的构造函数。
// 有关类定义的信息，请参阅 GSStrategy.h
CGSStrategy::CGSStrategy()
{
	return;
}

GSSTRATEGY_API IGSStrategy* CreateObject()
{
	if (!gGSStrategy)
		gGSStrategy = new CGSStrategy();

	return gGSStrategy;
}

GSSTRATEGY_API int ReleaseObject(IGSStrategy*pStrategy)
{
	if (gGSStrategy)
	{
		delete gGSStrategy;
		gGSStrategy = 0;
	}

	return SUCCESS;
}
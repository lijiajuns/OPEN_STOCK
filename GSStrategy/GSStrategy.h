#include "GSStrategy/Strategy.h"

#ifdef GSSTRATEGY_EXPORTS
#define GSSTRATEGY_API __declspec(dllexport)
#else
#define GSSTRATEGY_API __declspec(dllimport)
#endif

// 此类是从 GSStrategy.dll 导出的
class CGSStrategy :public IGSStrategy{
public:
	CGSStrategy(void);
public:
	// TODO:  在此添加您的方法。
	virtual int Analyse(STOCK_BEGIN nBegin,STOCK_CYCLE cycle, char *szOutFile);
private:
	CGSStrategy(const CGSStrategy&);
	CGSStrategy operator=(const CGSStrategy &);

public:
	void Storge(string code, tagStorgeItem &item);
private:
	int _Analyse();
	int _Step1();
	int _Step2();
	int _Step3();

private:
	string		m_szOutFile;
	STOCK_CYCLE	m_cycle;	//采用周期
	mStorgeItem m_stocks;
	mStorgeItem m_stocksA;		
	mWeight		m_weights;	//权重得分



};

GSSTRATEGY_API IGSStrategy* CreateObject();
GSSTRATEGY_API int ReleaseObject(IGSStrategy*pStrategy);

extern CGSStrategy * gGSStrategy;
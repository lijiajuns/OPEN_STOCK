#include "stdafx.h"
#include <algorithm>
#include "GSStrategy.h"
#include "common.h"
#include "GSTool/Tool.h"
#ifdef _DEBUG
#pragma comment(lib,"../lib/GSToolD.lib")
#else
#pragma comment(lib,"../lib/GSTool.lib")
#endif


int GSSqliteCall(void *pvoid, int argc, char **argv, char **azColName)
{
	if (argc != SQL_MAX)
	{
		return 0;
	}

	string szCode(argv[SQL_CODE]);
	tagStorgeItem item;
	item.volume = atoi(argv[SQL_VOLUME]);
	item.price = atof(argv[SQL_PRICE]);
	item.timestamp = atoi(argv[SQL_TIMESTAMP]);

	CGSStrategy* pA = (CGSStrategy*)pvoid;
	pA->Storge(szCode,item);

	return SUCCESS;
}

void CGSStrategy::Storge(string code, tagStorgeItem&item)
{
	m_stocks[code].push_back(item);
}


int CGSStrategy::Analyse(STOCK_BEGIN nBegin, STOCK_CYCLE cycle, char *szOutFile)
{
	m_cycle = cycle;
	m_szOutFile = szOutFile;

	if (!GSTOOL->ConnectSQLite())
	{
		return FAIL;
	}

	char sql[MAX_PATH]; 
	sprintf_s(sql, 256, "select stid,volume,new_price,timestamp from quote_0 where timestamp>='%d'", nBegin);
	if (FAIL == GSTOOL->ExecSql(sql, GSSqliteCall,this))
	{
		return FAIL;
	}

	return _Analyse();
}

/*
	自定义分析方法
*/

int CGSStrategy::_Analyse()
{
	int ret = SUCCESS;
	ret = (ret == SUCCESS) ? _Step1() : FAIL;
	ret = (ret == SUCCESS) ? _Step2() : FAIL;
	ret = (ret == SUCCESS) ? _Step3() : FAIL;
	return ret;
}

int CGSStrategy::_Step1()
{
	mStorgeItem &stocks = m_stocks;
	if (stocks.empty())
	{
		return FAIL;
	}

	//找到满足条件的K
	for (mStorgeItemIter it = stocks.begin(); it != stocks.end(); ++it)
	{
		tagStorgeItem un;
		un.price = it->second[0].price;
		un.volume = it->second[0].volume;
		un.timestamp = it->second[0].timestamp;
		m_stocksA[it->first].push_back(un);

		mStorgeItemIter itUnit = m_stocksA.find(it->first);
		if (itUnit != m_stocksA.end())
		{
			for (int i = 1; i < it->second.size(); i++)
			{
				int index = itUnit->second.size() - 1;
				tagStorgeItem &item = it->second[i];
				tagStorgeItem &un = itUnit->second[index];
				//过滤周期k
				if (item.timestamp >= un.timestamp + m_cycle)
				{
					tagStorgeItem un;
					un.price = item.price;
					un.volume = item.volume;
					un.timestamp = item.timestamp;
					itUnit->second.push_back(un);
				}
			}
		}
	}
	
	return m_stocksA.size() > 0 ?SUCCESS:FAIL;

}

int CGSStrategy::_Step2()
{
	for (mStorgeItemIter it = m_stocksA.begin(); it != m_stocksA.end(); ++it)
	{
		float volume_sp = 0.0f;
		float price_sp = 0.0f;

		for (int i = 1; i < it->second.size(); i++)
		{
			int volume = it->second[i - 1].volume;
			float price = it->second[i - 1].price;

			if (volume < 1 || price < 1)
			{
				break;
			}

			volume_sp = (it->second[i].volume - volume) / volume;
			price_sp = (it->second[i].price - price) / price;

			//额外放量 
			if (volume_sp >= SPEED_VOLUME_OFFSET)
			{
				m_weights[it->first].volume_weight += SPEED_VOLUME;
			}

			if (price_sp >= SPEED_PRICE_OFFSET)
			{
				m_weights[it->first].price_weight += SPEED_PRICE;
			}

			//减法
			if (volume_sp <= 0 - SPEED_VOLUME_OFFSET)
			{
				m_weights[it->first].volume_weight -= SPEED_VOLUME;
			}

			if (price_sp <= 0 - SPEED_VOLUME_OFFSET)
			{
				m_weights[it->first].price_weight -= SPEED_VOLUME;
			}
		}

	}

	return m_weights.size() > 0 ? SUCCESS : FAIL;
}

static int cmp(const PAIR& x, const PAIR& y)
{
	return x.second.volume_weight + x.second.price_weight> y.second.volume_weight + y.second.price_weight;
}

void sortMapByValue(mWeight& tMap, vPAIR& tVector)
{
	for (mWeightIter curr = tMap.begin(); curr != tMap.end(); curr++)
	{
		tVector.push_back(std::make_pair(curr->first, curr->second));
	}

	std::sort(tVector.begin(), tVector.end(), cmp);
}

int CGSStrategy::_Step3()
{
	FILE * fp;
	if ((fopen_s(&fp, m_szOutFile.c_str(), "w+")) != 0)
		return FAIL;

	printf("筛选结果：\n");
	fprintf_s(fp, "筛选结果:\n\n");


	//增幅榜
	int n = 0;
	vPAIR pairs;
	sortMapByValue(m_weights, pairs);

	for (int i = 0; i<pairs.size(); i++)
	{
		if (pairs[i].second.volume_weight >= SPEED_VOLUME && pairs[i].second.price_weight >= SPEED_PRICE)
		{
			n++;
			printf("%s,volume=%.2f,price=%.2f\n", pairs[i].first.c_str(), pairs[i].second.volume_weight, pairs[i].second.price_weight);
			fprintf_s(fp, "code=%s,volume = %.2f, price = %.2f\n", pairs[i].first.c_str(), pairs[i].second.volume_weight, pairs[i].second.price_weight);
		}
	}

	printf("\n\n放量榜,满足条件数量（%d）\n\n", n);
	fprintf_s(fp, "\n\n放量榜,满足条件数量（%d）AT(周期%d秒,加速度【%.2f,%.2f】)\n\n", n, m_cycle, SPEED_VOLUME_OFFSET, SPEED_PRICE_OFFSET);
	n = 0;
	//跌幅榜
	for (int i = 0; i < pairs.size(); i++)
	{
		if (pairs[i].second.volume_weight <= 0 - SPEED_VOLUME && pairs[i].second.price_weight <= 0 - SPEED_PRICE)
		{
			n++;
			printf("%s,volume=%.2f,price=%.2f\n", pairs[i].first.c_str(), pairs[i].second.volume_weight, pairs[i].second.price_weight);
			fprintf_s(fp, "code=%s,volume = %.2f, price = %.2f\n", pairs[i].first.c_str(), pairs[i].second.volume_weight, pairs[i].second.price_weight);
		}
	}

	printf("\n\n缩量榜,满足条件数量（%d）\n\n", n);
	fprintf_s(fp, "\n\n缩量榜,满足条件数量（%d）\AT(周期%d秒,加速度【%.2f,%.2f】)\n\n", n, m_cycle, SPEED_VOLUME_OFFSET, SPEED_PRICE_OFFSET);

	printf("筛选结束。\n\n");
	fprintf_s(fp, "筛选结束。\n\n");
	fclose(fp);

	return SUCCESS;
}

